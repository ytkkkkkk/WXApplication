package org.example.service;

import com.alibaba.fastjson2.JSON;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;
import com.alibaba.fastjson2.JSONObject;
import org.example.entity.WXArticles;
import org.example.util.HttpUtil;

public class WXService {

    private String WXToken="";

    public String init(String appid,String secret) throws Exception {

        String rs="";

        if(Objects.equals(appid, "") || Objects.equals(secret, "")){
            throw new Exception("appid或者secret为空!");
        }

        //创建请求
        String url="https://api.weixin.qq.com/cgi-bin/token?";
        HashMap hashMap = new HashMap<>();
        hashMap.put("appid", appid);
        hashMap.put("secret", secret);
        hashMap.put("grant_type", "client_credential");

        CloseableHttpClient httpClient = HttpClientBuilder.create().build();
        Iterator i = hashMap.keySet().iterator();
        while (i.hasNext()) {
            Object key=i.next();
            url=url+key.toString()+"="+hashMap.get(key)+"&";
        }
        System.out.println("请求地址===========》》》"+url);

        HttpGet get = new HttpGet(url);
        try{
            HttpResponse response = httpClient.execute(get);//执行获取响应
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK){//根据状态码处理
                //返回字符串
                rs = (String) EntityUtils.toString(response.getEntity());
                System.out.println("返回数据===========》》》"+rs);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                httpClient.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (JSONObject.parseObject(rs).getString("access_token").isEmpty()) {
            throw new Exception("获取微信token失败!");
        } else {
            WXToken = JSONObject.parseObject(rs).getString("access_token");
            System.out.println("获取到token：" + WXToken);
        }
        return WXToken;
    }

    /**
     * 文件上传到微信服务器的执行者
     * @return JSONObject
     * @throws Exception
     */
    public JSONObject uploadFile(String fileURL) throws Exception {

        String result=null;

        File file=new File(fileURL);
        URL urlObj = new URL("https://api.weixin.qq.com/cgi-bin/material/add_material?access_token="+this.WXToken+"&type=image");
        HttpURLConnection con = (HttpURLConnection) urlObj.openConnection();
        con.setRequestMethod("POST"); // 以Post方式提交表单，默认get方式
        con.setDoInput(true);
        con.setDoOutput(true);
        con.setUseCaches(false); // post方式不能使用缓存
        // 设置请求头信息
        con.setRequestProperty("Connection", "Keep-Alive");
        con.setRequestProperty("Charset", "UTF-8");
        // 设置边界
        String BOUNDARY = "----------" + System.currentTimeMillis();
        con.setRequestProperty("Content-Type", "multipart/form-data; boundary="+ BOUNDARY);
        // 请求正文信息
        // 第一部分：
        StringBuilder sb = new StringBuilder();
        sb.append("--"); // 必须多两道线
        sb.append(BOUNDARY);
        sb.append("\r\n");
        sb.append("Content-Disposition: form-data;name=\"media\";filename=\"a836b.jpg\"\r\n");
        sb.append("Content-Type:image/jpeg\r\n\r\n");
        byte[] head = sb.toString().getBytes("utf-8");
        // 获得输出流
        OutputStream out = new DataOutputStream(con.getOutputStream());
        // 输出表头
        out.write(head);
        // 文件正文部分
        // 把文件已流文件的方式 推入到url中
        DataInputStream in = new DataInputStream(new FileInputStream(file));
        int bytes = 0;
        byte[] bufferOut = new byte[1024];
        while ((bytes = in.read(bufferOut)) != -1) {
            out.write(bufferOut, 0, bytes);
        }
        in.close();
        // 结尾部分
        byte[] foot = ("\r\n--" + BOUNDARY + "--\r\n").getBytes("utf-8");// 定义最后数据分隔线
        out.write(foot);
        out.flush();
        out.close();
        StringBuffer buffer = new StringBuffer();
        BufferedReader reader = null;
        try {
            // 定义BufferedReader输入流来读取URL的响应
            reader = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String line = null;
            while ((line = reader.readLine()) != null) {
                //System.out.println(line);
                buffer.append(line);
            }
            if(result==null){
                result = buffer.toString();
            }
        } catch (IOException e) {
            e.printStackTrace();
            throw new IOException("上传文件异常");
        } finally {
            if(reader!=null){
                reader.close();
            }
        }
        System.out.println("上传结果："+JSONObject.parseObject(result));
        return JSONObject.parseObject(result);
    }

    /**
     * 微信新建草稿
     */
    public void addDraft(List<WXArticles> wxArticlesList) throws Exception {
        System.out.println("新增草稿>>>>>>>>"+wxArticlesList);
        JSONObject jsonObject=new JSONObject();
        jsonObject.put("articles",wxArticlesList);

        String rs= HttpUtil.postMethod("https://api.weixin.qq.com/cgi-bin/draft/add?access_token="+this.WXToken,jsonObject);
        String media_id=null;
        try{
            System.out.println("新建草稿结果："+JSON.parseObject(rs));
            media_id= JSON.parseObject(rs).get("media_id").toString();
        }catch (Exception e){
            media_id="feil to get media_id";
        }

    }

    public void submitDraft(String MediaId) throws Exception {
        JSONObject jsonObject=new JSONObject();
        jsonObject.put("media_id",MediaId);
        HttpUtil.postMethod("https://api.weixin.qq.com/cgi-bin/freepublish/submit?access_token="+this.WXToken,jsonObject);
    }
}
