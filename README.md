# 微信公众号接入工具

#### 介绍
微信公众号开放平台代码接入，详细教程请见微信公众号【小破孩的庄园】

#### 软件架构
软件架构说明
1. 项目本身非Web应用，可嵌入到自己的Web项目中
2. Java开发，教程简单，未使用Spring框架，自行接入就行
3. 遵循软件开发模式和基本原则


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  本项目是用于自己项目接入微信公众号开放平台的，针对现微信公众号开放的API接入
[微信公众开放文档地址](https://developers.weixin.qq.com/doc/offiaccount/Getting_Started/Overview.html)
2.  项目简单，可作为入门学习

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
